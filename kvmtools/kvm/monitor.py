#
# Module to handle the monitor stuff.
#

"""
(c) 2009-2017 Jens Kasten <jens@kasten-edv.de>
"""

import socket
import time
import logging 

class Monitor(object):
    """
    Class for connect and disconnect to a qemu monitor socket.
    Additional send data to and recieve data from monitor.
    """

    def __init__(self):
        self.socket = None
        self.socket_status = False
        self.recieve_data = {
            "data_offset_first_call": 2, 
            "data_offset_second_call": 1,
        } 
        # predefined qemu monitor options
        self.qemu_monitor = {
            "shutdown": "system_powerdown",
            "reboot": "sendkey ctrl-alt-delete 200",
            "enter": "sendkey ret",
            "status": "info status",
            "uuid": "info uuid",
            "network": "info network",
        }

    def open(self, options):        
        """Open a socket to connect to the qemu-kvm monitor."""
        if options.get('Type', None) == 'unix': 
            self.socket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
            try:
                self.socket.connect(options.get("SocketFile", None))
                return True
            except socket.error as error:
                logging.debug(error)
        else:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            try:
                self.socket.connect((options.get("Host", None), 
                    options.get('Port', None)))
                return True
            except socket.error as error:
                logging.debug(error)

    def close(self):
        """Close the opened socket connection."""    
        if self.socket is not None:
            self.socket.close()
            return True

    def send(self, command, raw=True):
        """Send data to socket."""
        if raw:
            command = '%s\n' % command

        if self.socket:
            try:
                self.socket.send(command.encode("utf-8"))
                time.sleep(0.2)
                return True
            except socket.error as error:
                if error[0] == 32:
                    logging.error("Could not send data to socket.")
                logging.error([1])

    def recieve(self, socket_buffer=4098):
        """Recieve data from socket and return it as a list."""
        result = []
        if self.socket:
            data = self.socket.recv(socket_buffer)
            if len(data) == 0:
                return no_result
            data = data.decode("utf-8").split("\r\n")
            # have to do this check because second call does not send
            # the qemu info string
            if data[0].startswith("QEMU"):
                counter = self.recieve_data['data_offset_first_call']
                if len(data) > self.recieve_data['data_offset_first_call']:
                    while counter < len(data) - 1:
                        result.append(data[counter])
                        counter += 1
                    if len(result) == 0:
                        result = ["Done"]
                    return result
            else:                    
                counter = self.recieve_data['data_offset_second_call']
                if len(data) > self.recieve_data['data_offset_second_call']:
                    while counter < len(data)-1:
                        result.append(data[counter])
                        counter += 1
                    return result

