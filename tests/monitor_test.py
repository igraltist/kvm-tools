import unittest

import subprocess 
import os
import time

from kvmtools import monitor

# have to fix to check which system is running
QEMU="qemu-system-x86_64"
OPTIONS = {
    "Type": "unix",
    "SocketFile": "/tmp/socketfile.sock",
    "Host": "localhost",
    "Port": 4444,
}
PID1 = "/tmp/qemu1.pid"
PID2 = "/tmp/qemu2.pid"


class TestMonitor(unittest.TestCase):

    def setUp(self):
        self.monitor = monitor.Monitor()

    def tearDown(self):
        if os.path.isfile(PID1):
            fd = open(PID1)
            pid = int(fd.read().strip())
            os.kill(pid, 9)
            os.remove(PID1) 
        if os.path.exists(OPTIONS["SocketFile"]):
            os.remove(OPTIONS["SocketFile"])        
        if os.path.isfile(PID2):
            fd = open(PID2)
            pid = int(fd.read().strip())
            os.kill(pid, 9)
            os.remove(PID2) 

    def test_connect_unix(self):
        cmd1 = [QEMU, "-monitor", 
            "unix:%s,server,nowait" % (OPTIONS["SocketFile"]),
            "-pidfile", PID1
        ]
        try:
            process = subprocess.Popen(cmd1)
            process.wait(3)
            self.assertEqual(self.monitor.open(OPTIONS), True)
            self.assertEqual(self.monitor.send("info status"), True)
            self.assertIs(type(self.monitor.recieve()), list)
            self.assertEqual(self.monitor.close(), True)
        except subprocess.TimeoutExpired as error:
            pass
    def test_tcp_connect_and_close(self):
        OPTIONS["Type"] = None
        cmd2 = [QEMU, "-monitor", 
            "tcp:%s:%d,server,nowait" % (OPTIONS["Host"], OPTIONS["Port"]),
            "-pidfile", PID2
        ]
        try:
            process = subprocess.Popen(cmd2)
            process.wait(3)
            self.assertEqual(self.monitor.open(OPTIONS), True)
            self.assertEqual(self.monitor.send("info status"), True)
            self.assertIs(type(self.monitor.recieve()), list)
            self.assertEqual(self.monitor.close(), True)
        except subprocess.TimeoutExpired as error:
            pass

if __name__ == "__main__":
    unittest.main(verbosity=2)
