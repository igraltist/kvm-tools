#!/usr/bin/env python
#

"""
(c) 2011-2017 Jens Kasten <jens.kasten@kasten-edv.de>
"""


import sys
import codecs
import os
import re

from setuptools import setup
from setuptools import find_packages
from setuptools.command.test import test as TestCommand


class PyTest(TestCommand):
    user_options = [('pytest-args=', 'a', "Arguments to pass to pytest")]

    def initialize_options(self):
        TestCommand.initialize_options(self)
        self.pytest_args = []

    def run_tests(self):
        import shlex
        #import here, cause outside the eggs aren't loaded
        import pytest
        errno = pytest.main(shlex.split(self.pytest_args))
        sys.exit(errno)

# take from pip
def read(*parts):
    here = os.path.abspath(os.path.dirname(__file__))
    # intentionally *not* adding an encoding option to open, See:
    # https://github.com/pypa/virtualenv/issues/201#issuecomment-3145690
    return codecs.open(os.path.join(here, *parts), 'r').read()

def find_version(*file_paths):
    version_file = read(*file_paths)
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")


# setup a list to scripts which are run as standalone 
scripts = ["scripts/kvm-admin", "scripts/generate-kvm-options",
    "scripts/kvm-ifup", "scripts/kvm-ifdown"]

setup(
    name="kvm-tools",
    version=find_version("kvmtools", "__version__.py"),
    long_description=read("README.md"),
    description="Command-line tools for kvm or qemu guests.",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: System Administrators",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 2.7"
    ],
    keywords=["kvm-admin", "kvmtools"],
    author="Jens Kasten",
    author_email="jens.kasten@kasten-edv.de",
    url="http://bitbucket.org/igraltist/kvm-tools",
    license="GNU GPLv3",
    packages=find_packages(),
    scripts=scripts,
    #tests_require=['pytest'],
    #cmdclass = {'test': PyTest},
)
