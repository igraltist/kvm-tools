.. _`Installation`:

Installation
============

I drop python2 support.
Now latest tested version runs on python3.4.


.. _`get-kvmtools`:

1. `clone repository`_
2. `download archive`_
3. `Package Manager`_

.. _`clone repository`: 

1. clone repository
------------------- 

Install::

  hg clone https://hg.kasten-edv.de/kvm-tools
  cd kvm-tools 
  python setup.py install or pip install .

Upgrade::

  pip install --upgrade .

.. _`download archive`:

2. download archive
-------------------

Install::

  wget https://hg.kasten-edv.de/kvm-tools/get/tip.tar.bz2 
  # for alternative archive use zip or gz as suffix
  tar xvjf tip.tar.bz2
  cd kvm-tools
  pip install .

  

.. _pip:

3. pip
------

Install:: 

  pip install kvm-tools

Upgrade::
        
  pip install --upgrade kvm-tools

